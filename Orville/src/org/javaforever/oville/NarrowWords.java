package org.javaforever.oville;

import java.util.ArrayList;
import java.util.List;

public class NarrowWords {
	private String varName;
	private List<NarrowWord> narrowWords = new ArrayList<NarrowWord>();
	public String getVarName() {
		return varName;
	}
	public void setVarName(String varName) {
		this.varName = varName;
	}
	public List<NarrowWord> getNarrowWords() {
		return narrowWords;
	}
	public void setNarrowWords(List<NarrowWord> narrowWords) {
		this.narrowWords = narrowWords;
	}
	public void addNarrowWord(NarrowWord nw) {
		if (this.narrowWords != null) this.narrowWords.add(nw);
		else {
			this.narrowWords = new ArrayList<NarrowWord>();
			this.narrowWords.add(nw);
		}
	}
	
	public NarrowWord getNarrowWord(int pos) {
		if (pos < 0 ||pos >= this.getNarrowWords().size()) return null;
		else return this.getNarrowWords().get(pos);
	}
	
	public String toHexStr() throws ValidateException{
		StringBuilder sb = new StringBuilder();
		for (NarrowWord nw:this.getNarrowWords()) {
			sb.append(nw.toHexStr()+"\n");
		}
		return sb.toString();
	}
	
	public String toBinStr() throws ValidateException{
		StringBuilder sb = new StringBuilder();
		for (NarrowWord nw:this.getNarrowWords()) {
			sb.append(nw.toBinStr()+"\n");
		}
		return sb.toString();
	}
	
	public NarrowWord sum() throws ValidateException{
		long sum = 0;
		for (NarrowWord nw:this.narrowWords) {
			sum += nw.toLong();
		}
		return NarrowWord.fromLong(sum);
	}
	
	public StatementList toStatementList() throws ValidateException{
		StatementList stls = new StatementList();
		for (int i=0;i<this.getNarrowWords().size();i++) {
			stls.add(new Statement(i*1000L, this.getNarrowWords().get(i).toBinStr()));
		}
		return stls;
	}	
	
	public boolean equals(NarrowWords nwl) throws ValidateException{
		if (this.getNarrowWords().size() != nwl.getNarrowWords().size()) return false;
		for (int i=0;i<this.getNarrowWords().size();i++) {
			if (!this.getNarrowWord(i).equals(nwl.getNarrowWord(i))) return false;
		}
		return true;
	}
	
	public static void main(String [] args) throws ValidateException{
		NarrowWords nws = new NarrowWords();
		for (int i=0;i<100;i++) {
			nws.addNarrowWord(NarrowWord.fromLong(i+1));
		}
		System.out.println(nws.sum().toLong());
	}
}
