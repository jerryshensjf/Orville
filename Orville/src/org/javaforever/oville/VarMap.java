package org.javaforever.oville;

import java.util.Map;
import java.util.TreeMap;

public class VarMap {
	protected Map<String, Var> varMap = new TreeMap<>();
	protected String varName = "varMap";

	public Map<String, Var> getVarMap() {
		return varMap;
	}

	public void setVarMap(Map<String, Var> varMap) {
		this.varMap = varMap;
	}
	
	public void putVar(String varName, Var var) {
		this.varMap.put(varName, var);
	}
	
	public Var getVar(String varName) {
		return this.varMap.get(varName);
	}

	public String getVarName() {
		return varName;
	}

	public void setVarName(String varName) {
		this.varName = varName;
	}
	
	public String toStr() throws ValidateException{
		StringBuilder sb = new StringBuilder();
		for (String key:this.varMap.keySet()) {
			sb.append(varMap.get(key).toStr()).append("\n");
		}
		return sb.toString();
	}
}
