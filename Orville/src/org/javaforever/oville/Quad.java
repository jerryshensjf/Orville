package org.javaforever.oville;

public class Quad {
	private String varName;
	private byte quad = 0;
	private int mask = 0;
	public int getMask() {
		return mask;
	}
	public void setMask(int mask) {
		this.mask = mask;
	}
	public String getVarName() {
		return varName;
	}
	public void setVarName(String varName) {
		this.varName = varName;
	}

	public String toBinStr() throws ValidateException{
		StringBuilder sb = new StringBuilder();
		byte highbit = (byte)((quad >>> (2*mask+1))%2);
		if (highbit<0) highbit += 2;
		byte lowbit = (byte)((quad >>> (2*mask))%2);
		if (lowbit<0) lowbit += 2;
		//System.out.println(bit);
		sb.append(highbit).append(lowbit);
		return sb.toString();
	}
	
	public String toQuadStr() throws ValidateException{
		String bin = this.toBinStr();
		StringBuilder sb = new StringBuilder();
		sb.append(StringUtil.binDigToQuad(bin.substring(0,2)));
		return sb.toString();
	}	
	
	public byte getQuad() {
		return quad;
	}
	public void setQuad(byte quad, int mask) {
		this.quad = quad;
		this.mask = mask%4;
	}
	
	public Quad(byte quad, int mask) {
		super();
		this.quad = quad;
		this.mask = mask%4;
	}
	
	public Quad(String qName) {
		super();
		if (qName.equals("0")) {
			this.quad = 0b00000000;
			this.mask = 0;
		}else if (qName.equals("1")) {
			this.quad = 0b00000001;
			this.mask = 0;
		}else if (qName.equals("Q")) {
			this.quad = 0b00000010;
			this.mask = 0;
		}else if (qName.equals("P")) {
			this.quad = 0b00000011;
			this.mask = 0;
		}
	}
	
	public Quad(char qName) {
		super();
		if (qName=='0') {
			this.quad = 0b00000000;
			this.mask = 0;
		}else if (qName=='1') {
			this.quad = 0b00000001;
			this.mask = 0;
		}else if (qName=='Q') {
			this.quad = 0b00000010;
			this.mask = 0;
		}else if (qName=='P') {
			this.quad = 0b00000011;
			this.mask = 0;
		}
	}

	public static void main(String[] argv) throws ValidateException{		
		byte bb = 0b01010111;
		Quad qd = new Quad(bb,1);
		qd.setVarName("quad");
		
		System.out.println(qd.toBinStr());
		System.out.println(qd.toQuadStr());
	}

}
