package org.javaforever.oville;

public class Script {	
	protected StatementList statements;
	protected String fileName = "script.qa";
	protected String progName = "Script";
	protected VarMap results = new VarMap();
	
	public StatementList getStatements() {
		return statements;
	}
	public void setStatements(StatementList statements) {
		this.statements = statements;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getProgName() {
		return progName;
	}
	public void setProgName(String progName) {
		this.progName = progName;
	}
	public VarMap getResults() {
		return results;
	}
	public void setResults(VarMap results) {
		this.results = results;
	}

}
