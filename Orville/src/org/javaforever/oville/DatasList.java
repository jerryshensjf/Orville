package org.javaforever.oville;

import java.util.List;

public class DatasList {
	protected String dataListName;
	protected List<Words> wordsList;
	protected List<NarrowWords> narrowWordsList;
	protected List<TextWords> textWordsList;
	protected String dataListType = "WordsList";
	public String getDataListName() {
		return dataListName;
	}
	public void setDataListName(String dataListName) {
		this.dataListName = dataListName;
	}
	public List<Words> getWordsList() {
		return wordsList;
	}
	public void setWordsList(List<Words> wordsList) {
		this.wordsList = wordsList;
	}
	public List<NarrowWords> getNarrowWordsList() {
		return narrowWordsList;
	}
	public void setNarrowWordsList(List<NarrowWords> narrowWordsList) {
		this.narrowWordsList = narrowWordsList;
	}
	public List<TextWords> getTextWordsList() {
		return textWordsList;
	}
	public void setTextWordsList(List<TextWords> textWordsList) {
		this.textWordsList = textWordsList;
	}
	public String getDataListType() {
		return dataListType;
	}
	public void setDataListType(String dataListType) {
		this.dataListType = dataListType;
	}
}
