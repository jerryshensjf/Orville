package org.javaforever.oville;

public class Function {	
	protected StatementList statements;
	protected String progName = "func1";
	protected VarMap results = new VarMap();
	protected VarMap params = new VarMap();
	
	public StatementList getStatements() {
		return statements;
	}
	public void setStatements(StatementList statements) {
		this.statements = statements;
	}
	public String getProgName() {
		return progName;
	}
	public void setProgName(String progName) {
		this.progName = progName;
	}
	public VarMap getResults() {
		return results;
	}
	public void setResults(VarMap results) {
		this.results = results;
	}

}
