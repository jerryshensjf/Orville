package org.javaforever.oville;

import java.util.ArrayList;
import java.util.List;

public class TextWord {
	private String varName;
	private char [] textWord = new char[32];
	public String getVarName() {
		return varName;
	}
	public void setVarName(String varName) {
		this.varName = varName;
	}
	
	public char[] getTextWord() {
		return textWord;
	}
	
	public void setTextWord(char[] textWord) {
		this.textWord = textWord;
	}
	
	public char getChar(int pos) {
		return this.getTextWord()[pos%32];
	}
	
	public String toStr() throws ValidateException{
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<32;i++) {
			char c = textWord[i];
			sb.append(c);
		}
		return sb.toString();
	}
	
	public static TextWord fromStr(String str) throws ValidateException{
		if (str.length() != 32) throw new ValidateException("Wrong text word string length!");
		TextWord tw = new TextWord();
		char [] cc = new char[32];
		for (int i=0;i<32;i++) {
			cc [i] = str.charAt(i);
		}
		tw.setTextWord(cc);
		return tw;
	}
	
	public Word toWord(String pattern) throws ValidateException {
		if (pattern.length()!=4) throw new  ValidateException("Wrong word pattern!");
		String quadString = this.toStr();
		quadString = quadString.replace(pattern.substring(0,1),"0");
		quadString = quadString.replace(pattern.substring(1,2),"1");
		quadString = quadString.replace(pattern.substring(2,3),"Q");
		quadString = quadString.replace(pattern.substring(3,4),"P");
		//System.out.println(quadString);
		return Word.fromQuadStr(quadString);
	}
	
	public NarrowWord toNarrowWord(String pattern) throws ValidateException {
		if (pattern.length()!=2) throw new  ValidateException("Wrong narrow word pattern!");
		String quadString = this.toStr();
		quadString = quadString.replace(pattern.substring(0,1),"0");
		quadString = quadString.replace(pattern.substring(1,2),"1");
		System.out.println(quadString);
		return NarrowWord.fromBinStr(quadString);
	}
	
	public TextWord replace(String pattern1,String pattern2) throws ValidateException {
		if (pattern1.length()!= pattern2.length()) throw new  ValidateException("Wrong text word replace patterns!");
		String quadString = this.toStr();
		for (int i=0;i<pattern1.length();i++) {
			quadString = quadString.replace(pattern1.substring(i,i+1),pattern2.substring(i,i+1));
		}
		System.out.println(quadString);
		return TextWord.fromStr(quadString);
	}
	
	public static TextWord oneCharTextWord(char c) throws ValidateException{
		StringBuilder sb = new StringBuilder();
		for (int i=0;i<32;i++) {
			sb.append(c);
		}
		return TextWord.fromStr(sb.toString());
	}
	
	public void setChar(int col,char ch) throws ValidateException{
		if (col <0 ||col >31) throw new  ValidateException("Wrong textWord position!");
		else {
			this.textWord[col] = ch;
		}
	}
	
	public boolean equals(TextWord tw) throws ValidateException{
		return this.toStr().equals(tw.toStr());
	}
	
	public static void main(String[] argv) throws ValidateException{
		TextWord tw = TextWord.fromStr("************S***********E*******");
		tw.setVarName("textWord");
		System.out.println(tw.toStr());
		System.out.println(tw.toWord("* SE").toQuadStr());
		
		TextWord tw2 = TextWord.fromStr("************E***********E*******");
		tw.setVarName("textWord2");
		System.out.println(tw2.toStr());
		System.out.println(tw2.toNarrowWord("*E").toBinStr());
		System.out.println(tw2.toNarrowWord("*E").toHexStr());
	}
}
