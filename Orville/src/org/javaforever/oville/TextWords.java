package org.javaforever.oville;

import java.util.ArrayList;
import java.util.List;

public class TextWords {
	private String varName;
	private List<TextWord> textWords = new ArrayList<TextWord>();
	public String getVarName() {
		return varName;
	}
	public void setVarName(String varName) {
		this.varName = varName;
	}

	public List<TextWord> getTextWords() {
		return textWords;
	}
	
	public void setTextWords(List<TextWord> textWords) {
		this.textWords = textWords;
	}
	
	public void addTextWord(TextWord tw) {
		if (this.textWords != null) this.textWords.add(tw);
		else {
			this.textWords = new ArrayList<TextWord>();
			this.textWords.add(tw);
		}
	}
	
	public String toStr() throws ValidateException{
		StringBuilder sb = new StringBuilder();
		for (TextWord tw:this.getTextWords()) {
			sb.append(tw.toStr()).append("\n");
		}
		return sb.toString();
	}
	
	public Words toWords(String pattern) throws ValidateException{
		Words ws = new Words();
		ws.setVarName(this.varName);
		List<Word> words = new ArrayList<>();
		for (TextWord tw:this.textWords) {
			Word w = tw.toWord(pattern);
			words.add(w);
		}
		ws.setWords(words);
		return ws;		
	}
	
	public TextWords fromWords(Words words,String pattern) throws ValidateException{
		TextWords tws = new TextWords();
		tws.setVarName(this.varName);
		for (Word w: words.getWords()) {
			TextWord tw = w.toTextWord(pattern);
			tws.addTextWord(tw);
		}
		return tws;		
	}
	
	public TextWords replace(String pattern1,String pattern2) throws ValidateException{
		TextWords tws = new TextWords();
		tws.setVarName(this.varName);
		for (TextWord tw: this.getTextWords()) {
			TextWord tw2 = tw.replace(pattern1,pattern2);
			tws.addTextWord(tw2);
		}
		return tws;		
	}
	
	public NarrowWords toNarrowWords(String pattern) throws ValidateException{
		NarrowWords nws = new NarrowWords();
		nws.setVarName(this.varName);
		List<NarrowWord> nwords = new ArrayList<>();
		for (TextWord tw:this.textWords) {
			NarrowWord nw = tw.toNarrowWord(pattern);
			nwords.add(nw);
		}
		nws.setNarrowWords(nwords);
		return nws;		
	}
	
	public Position locateOnlyChar(char c) {
		int x = -1;
		int y = -1;
		for (int j=0;j<this.textWords.size();j++) {
			for (int i=0;i<32;i++) {
				if (c == this.textWords.get(j).getTextWord()[i]) {
					x = i;
					y = j;
				}
			}
		}
		return new Position(x,y);
	}
	
	public void setChar(int col,int row,char ch) throws ValidateException{
		if (col<0||col>=32) throw new ValidateException("Wrong textWords col!");
		else if (row<0||row>=this.textWords.size()) throw new ValidateException("Wrong textWords row!");
		this.textWords.get(row).setChar(col, ch);
	}
	
	public static TextWords oneCharTextWords(int rows,char ch) throws ValidateException{
		TextWords tws = new TextWords();
		for (int i=0;i<rows;i++) {
			tws.addTextWord(TextWord.oneCharTextWord(ch));
		}
		return tws;
	}
	
	public StatementList toStatementList() throws ValidateException{
		StatementList stls = new StatementList();
		for (int i=0;i<this.getTextWords().size();i++) {
			stls.add(new Statement(i*1000L, this.getTextWords().get(i).toStr()));
		}
		return stls;
	}
	
	public TextWord getTextWord(int pos) {
		if (pos < 0 ||pos >= this.getTextWords().size()) return null;
		else return this.getTextWords().get(pos);
	}
	
	public boolean equals(TextWords twl) throws ValidateException{
		if (this.getTextWords().size() != twl.getTextWords().size()) return false;
		for (int i=0;i<this.getTextWords().size();i++) {
			if (!this.getTextWord(i).equals(twl.getTextWord(i))) return false;
		}
		return true;
	}
	
	public static void main(String [] args) throws ValidateException{
		TextWords tws = new TextWords();
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("**S**************************E**"));
		tws.addTextWord(TextWord.fromStr("** ************************** **"));
		tws.addTextWord(TextWord.fromStr("** ************************** **"));
		tws.addTextWord(TextWord.fromStr("** ************************** **"));
		tws.addTextWord(TextWord.fromStr("**                            **"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		tws.addTextWord(TextWord.fromStr("********************************"));
		
		System.out.println(tws.toStr());
		System.out.println(tws.getTextWords().size());
		System.out.println(tws.toWords("* SE").toQuadStr());
		System.out.println(tws.locateOnlyChar('S').getX()+":"+tws.locateOnlyChar('S').getY());
		System.out.println(tws.locateOnlyChar('E').getX()+":"+tws.locateOnlyChar('E').getY());
		TextWords tws2 = TextWords.oneCharTextWords(32, 'Q');
		Position pos = tws.locateOnlyChar('S');
		Position pos2 = tws.locateOnlyChar('E');
		tws2.setChar(pos.getX(), pos.getY(), 'S');
		tws2.setChar(pos2.getX(), pos2.getY(), 'E');
		System.out.println(tws2.toStr());
	}
}
