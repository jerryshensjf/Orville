package org.javaforever.oville;

public class Var implements Comparable<Var>{
	private String varName;
	private Words words;
	private NarrowWords narrowWords;
	private TextWords textWords;
	private Word word;
	private NarrowWord narrowWord;
	private TextWord textWord;
	private DatasList datasList;
	private Statement statement;
	private StatementList statementList;
	private String varType = "Words";
	
	public DatasList getDatasList() {
		return datasList;
	}
	public void setDatasList(DatasList datasList) {
		this.varType = "DatasList";
		this.datasList = datasList;
	}
	
	public TextWords getTextWords() {
		return textWords;
	}
	public void setTextWords(TextWords textWords) {
		this.varType = "TextWords";
		this.textWords = textWords;
	}
	public Word getWord() {
		return word;
	}
	public void setWord(Word word) {
		this.varType = "Word";
		this.word = word;
	}
	public NarrowWord getNarrowWord() {
		return narrowWord;
	}
	public void setNarrowWord(NarrowWord narrowWord) {
		this.varType = "NarrowWord";
		this.narrowWord = narrowWord;
	}
	public TextWord getTextWord() {
		return textWord;
	}
	public void setTextWord(TextWord textWord) {
		this.varType = "TextWord";
		this.textWord = textWord;
	}
	public String getVarName() {
		return varName;
	}
	public void setVarName(String varName) {
		this.varName = varName;
	}
	public Words getWords() {
		return words;
	}
	public void setWords(Words words) {
		this.varType = "Words";
		this.words = words;
	}
	public NarrowWords getNarrowWords() {
		return narrowWords;
	}
	public void setNarrowWords(NarrowWords narrowWords) {
		this.varType = "NarrowWords";
		this.narrowWords = narrowWords;
	}
	public String getVarType() {
		return varType;
	}
	public void setVarType(String varType) {
		this.varType = varType;
	}
	@Override
	public int compareTo(Var o) {
		return this.varName.compareTo(o.getVarName());
	}
	
	public boolean equals(Var var) throws ValidateException {
		if (!this.varType.equals(var.getVarType())) return false;
		if ("NarrowWord".equals(this.varType)) return this.getNarrowWord().equals(var.getNarrowWord());
		if ("Word".equals(this.varType)) return this.getWord().equals(var.getWord());
		if ("TextWord".equals(this.varType)) return this.getTextWord().equals(var.getTextWord());
		if ("NarrowWords".equals(this.varType)) return this.getNarrowWords().equals(var.getNarrowWords());
		if ("Words".equals(this.varType)) return this.getWords().equals(var.getWords());
		if ("TextWords".equals(this.varType)) return this.getTextWords().equals(var.getTextWords());
		if ("Statement".equals(this.varType)) return this.getStatement().equals(var.getStatement());
		if ("StatementList".equals(this.varType)) return this.getStatementList().equals(var.getStatementList());
		return false;
	}
	
	
	public String toStr() throws ValidateException {
		switch (varType) {
		case "Word" :
			return this.word.toQuadStr();
		case "NarrowWord" :
			return this.narrowWord.toBinStr();
		case "TextWord" :
			return this.textWord.toStr();
		case "Words" :
			return this.words.toQuadStr();
		case "NarrowWords" :
			return this.narrowWords.toBinStr();
		case "TextWords" :
			return this.textWords.toStr();
		case "Statement" :
			return this.statement.getContent();
		case "StatementList" :
			return this.statementList.getContent();
		}
		throw new ValidateException("Not correct var type.");
	}
	
	public StatementList toStatementList() throws ValidateException {
		switch (varType) {
		case "Word" :
			return StatementList.fromStr(this.word.toQuadStr());
		case "NarrowWord" :
			return StatementList.fromStr(this.narrowWord.toBinStr());
		case "TextWord" :
			return StatementList.fromStr(this.textWord.toStr());
		case "Words" :
			return this.words.toStatementList();
		case "NarrowWords" :
			return this.narrowWords.toStatementList();
		case "TextWords" :
			return this.textWords.toStatementList();
		case "Statement" :
			return this.statement.toStatementList();
		case "StatementList" :
			return this.statementList;
		}
		throw new ValidateException("Not correct var type.");
	}
	
	public Statement getStatement() {
		return statement;
	}
	public void setStatement(Statement statement) {
		this.varType = "Statement";
		this.statement = statement;
	}
	public StatementList getStatementList() {
		return statementList;
	}
	public void setStatementList(StatementList statementList) {
		this.varType = "StatementList";
		this.statementList = statementList;
	}
}
