package org.javaforever.oville;

import java.util.ArrayList;
import java.util.List;

public class Words {
	private String varName;
	private List<Word> words = new ArrayList<Word>();
	public String getVarName() {
		return varName;
	}
	public void setVarName(String varName) {
		this.varName = varName;
	}
	public List<Word> getWords() {
		return words;
	}
	public void setWords(List<Word> words) {
		this.words = words;
	}
	public void addWord(Word w) {
		if (this.words != null) this.words.add(w);
		else {
			this.words = new ArrayList<Word>();
			this.words.add(w);
		}
	}
	
	public String toQuadStr() throws ValidateException{
		StringBuilder sb = new StringBuilder();
		for (Word w:this.getWords()) {
			sb.append(w.toQuadStr()).append("\n");
		}
		return sb.toString();
	}
	
	public StatementList toStatementList() throws ValidateException{
		StatementList stls = new StatementList();
		for (int i=0;i<this.getWords().size();i++) {
			stls.add(new Statement(i*1000L, this.getWords().get(i).toQuadStr()));
		}
		return stls;
	}
	
	public Word getWord(int pos) {
		if (pos < 0 ||pos >= this.getWords().size()) return null;
		else return this.getWords().get(pos);
	}
	
	public boolean equals(Words wl) throws ValidateException{
		if (this.getWords().size() != wl.getWords().size()) return false;
		for (int i=0;i<this.getWords().size();i++) {
			if (!this.getWord(i).equals(wl.getWord(i))) return false;
		}
		return true;
	}
}
