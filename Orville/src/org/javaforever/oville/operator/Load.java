package org.javaforever.oville.operator;

import java.io.FileReader;
import java.util.Scanner;

import org.javaforever.oville.Statement;
import org.javaforever.oville.StatementList;
import org.javaforever.oville.TextWord;
import org.javaforever.oville.TextWords;
import org.javaforever.oville.ValidateException;
import org.javaforever.oville.ValidateInfo;

public class Load {
	public static StatementList load(String filePath) throws Exception{
		StatementList stats = new StatementList();
		long serial = 0L;
	   try (Scanner sc = new Scanner(new FileReader(filePath))) {
	      while (sc.hasNextLine()) {  //按行读取字符串
	         String line = sc.nextLine();
	         //System.out.println(line);
	         Statement st = new Statement(serial,line);
	         stats.add(st);
	         serial += 1000L;
	      }
	   }
	   return stats;
	}
	
	public static TextWords loadData(String filePath) throws Exception{
		StatementList stats = load(filePath);
		ValidateInfo infos = new ValidateInfo();
		TextWords textWords = new TextWords();
		for (Statement st : stats) {
			if(st.getContent().length()!=32) infos.addCompileError("Wrong text word "+st.getContent());
			else textWords.addTextWord(TextWord.fromStr(st.getContent()));
		}
		if (!infos.isSuccess(false)) throw new ValidateException(infos);
		else return textWords;
	}
	
	public static TextWords loadData(StatementList stats) throws Exception{
		ValidateInfo infos = new ValidateInfo();
		TextWords textWords = new TextWords();
		for (Statement st : stats) {
			if(st.getContent().length()!=32) infos.addCompileError("Wrong text word "+st.getContent());
			else textWords.addTextWord(TextWord.fromStr(st.getContent()));
		}
		if (!infos.isSuccess(false)) throw new ValidateException(infos);
		else return textWords;
	}
	
	public static void main(String [] argv) {
		try {
			TextWords tws = loadData("scripts/sampleData1.data");
			System.out.println(tws.toStr());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
