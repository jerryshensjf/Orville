package org.javaforever.oville.operator;

import java.io.FileWriter;
import java.io.IOException;

import org.javaforever.oville.Statement;
import org.javaforever.oville.StatementList;
import org.javaforever.oville.TextWord;
import org.javaforever.oville.TextWords;
import org.javaforever.oville.ValidateException;

public class Write {
	public static void output(StatementList statements,String filePath) throws ValidateException, IOException{
		try (FileWriter fileWriter = new FileWriter(filePath,true)) { 
			for (Statement st:statements) {
				fileWriter.append(st.getContent()).append("\n"); 
			}
	    } 
	}
	
	public static void output(Statement statement,String filePath) throws ValidateException, IOException{
		try (FileWriter fileWriter = new FileWriter(filePath,true)) { 
			fileWriter.append(statement.getContent()); 
	    } 
	}
	
	public static void output(TextWords textWords,String filePath) throws ValidateException, IOException{
		try (FileWriter fileWriter = new FileWriter(filePath,true)) { 
			fileWriter.append(textWords.toStr()); 
	    } 
	}
	
	public static void output(TextWord textWord,String filePath) throws ValidateException, IOException{
		try (FileWriter fileWriter = new FileWriter(filePath,true)) { 
			fileWriter.append(textWord.toStr()); 
	    } 
	}
}
